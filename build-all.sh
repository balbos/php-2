#!/bin/bash
docker build -t nettematic/php:5.5 ./5.5
docker build -t nettematic/php:5.5-fpm ./5.5-fpm
docker build -t nettematic/php:5.6 ./5.6
docker build -t nettematic/php:5.6-fpm ./5.6-fpm
docker build -t nettematic/php:7.0 ./7.0
docker build -t nettematic/php:7.0-fpm ./7.0-fpm
docker build -t nettematic/php:7.1 ./7.1
docker build -t nettematic/php:7.1-fpm ./7.1-fpm
docker build -t nettematic/php:7.2 ./7.2
docker build -t nettematic/php:7.2-fpm ./7.2-fpm
