[![Docker Stars](https://img.shields.io/docker/stars/nettematic/php.svg?style=flat)](https://hub.docker.com/r/nettematic/php/) [![Docker Pulls](https://img.shields.io/docker/pulls/nettematic/php.svg?style=flat)](https://hub.docker.com/r/nettematic/php/)

Not for production use! Created for testing!
Based on https://github.com/dockette/php ... Thanks F3l1x


# PHP

PHP 5.5 / 5.6 / 7.0 / 7.1 / 7.2, with FPM and Composer and Supervisor.

-----
## Obrazy
V obrazech je předinstalována řada PHP rozšíření

| PHP       | PHP+FPM   | 
|-----------|-----------|
| apc       | apc       | 
| apcu      | apcu      | 
| bcmath    | bcmath    | 
| bz2       | bz2       | 
| calendar  | calendar  | 
| cgi       | cgi       | 
| cli       | cli       | 
| ctype     | ctype     | 
| curl      | curl      | 
| geoip     | geoip     | 
| gettext   | gettext   | 
| gd        | gd        | 
| -         | fpm       | 
| intl      | intl      | 
| imagick   | imagick   | 
| imap      | imap      | 
| ldap      | ldap      | 
| mbstring  | mbstring  | 
| mcrypt    | mcrypt    | 
| memcached | memcached | 
| mongo     | mongo     |
| mysql     | mysql     |
| pdo       | pdo       | 
| pgsql     | pgsql     | 
| redis     | redis     | 
| soap      | soap      | 
| sqlite3   | sqlite3   | 
| ssh2      | ssh2      | 
| xmlrpc    | xmlrpc    | 
| xsl       | xsl       | 
| zip       | zip       | 
| json      | json      |

##Použití
